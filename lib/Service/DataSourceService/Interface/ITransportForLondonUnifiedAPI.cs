﻿using System.Threading.Tasks;

namespace DataSourceService.Interface
{
    public interface ITransportForLondonUnifiedApi
    {
        Task<string> AirQuality();
        Task<string> BikePoint();
        Task<string> BikePoint(string id);
        Task<string> BikePoint(string swLat, string swLon, string neLat, string neLon);
        Task<string> BikePoint(string lat, string lon, string radius);
        Task<string> BikePointByName(string name);
    }
}