﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataSourceService.Abstract;
using DataSourceService.Interface;

namespace DataSourceService
{
    public class TransportLondonApi : BaseRequestAbstract, ITransportForLondonUnifiedApi
    {
        public TransportLondonApi(string appId, string appKey, string servicePrefix) : base(appId, appKey, servicePrefix)
        {}

        #region AirQuality

        public Task<string> AirQuality() =>
            Request("/AirQuality");

        #endregion

        #region BikePoint

        public Task<string> BikePoint() =>
            Request("/BikePoint");

        public Task<string> BikePoint(string id) =>
            Request($"/BikePoint/{id}");

        public Task<string> BikePoint(string swLat, string swLon, string neLat, string neLon) =>
            Request($"/BikePoint?swLat={swLat}&swLon={swLon}&neLat={neLat}&neLon={neLon}");

        public Task<string> BikePoint(string lat, string lon, string radius) =>
            Request($"/BikePoint?lat={lat}&lon={lon}&radius={radius}");

        public Task<string> BikePointByName(string name) =>
            Request($"/BikePoint/Search?query={name}");

        #endregion
    }
}
