﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataSourceService.Abstract
{
    public abstract class BaseRequestAbstract
    {
        // Configuration
        private readonly string _appId;
        private readonly string _appKey;
        private readonly string _servicePrefix;

        protected BaseRequestAbstract(string appId, string appKey, string servicePrefix)
        {
            _appId = appId;
            _appKey = appKey;
            _servicePrefix = servicePrefix;
        }

        protected async Task<string> Request(string request)
        {
            var url = request.Contains("?")
                ? $"{_servicePrefix}/{request}&app_id={_appId}&app_key={_appKey}"
                : $"{_servicePrefix}/{request}?app_id={_appId}&app_key={_appKey}";

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(url);

                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    throw new NotFoundException();
                }
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }
                throw new Exception(response.ReasonPhrase);
            }
        }
    }
}