﻿// Simple Angular Application

// Creating App
var app = angular.module("App", []);

// Search panel
app.directive("searchPanel", ['dataFactory', function (dataFactory) {
    return {
        templateUrl: 'Views/search.html',
        restrict: 'E',
        scope: {},
        link: function (scope, element, attrs) {
            console.log(dataFactory);

            // private functions
            var $$ResponceOk;
            var $$ResponceError;
            var $$button;

            // base Scope functions
            scope.NotFound = false;
            scope.MenuItem = 1;
            scope.Header = "AirQuality";
            //scope.Data = {};
            scope.Text = 'Sample ...';
            scope.Busy = false;
            //scope.Search = function() {
            //    scope.Data = dataFactory.SearchData(scope.Text);
            //}

            //change menu
            scope.ChangeMenu = function ($event, menuitem) {
                scope.MenuItem = menuitem;
                scope.NotFound = false;
                scope.Header = angular.element($event.currentTarget).text();
                if (menuitem !== 1) {
                    delete scope.Data;
                }
            }

            // Get Air Quality
            scope.GetAir = function ($event) {
                // prevent double run
                if (scope.Busy) return;
                scope.Busy = true;

                //Get current button
                var button = $event.currentTarget;

                // Disable it
                angular.element(button).addClass("disabled");

                $event.preventDefault();
                var promice = dataFactory.GetAir();
                promice.then(function (responce) {
                    scope.Busy = false;

                    // remove Disabled
                    angular.element(button).removeClass("disabled");

                    scope.AirInfo = responce.data;
                    console.log(responce);
                }, function (responce) {
                    alert('Error!');
                    scope.Busy = false;

                    //remove Disabled
                    angular.element(button).removeClass("disabled");

                    delete scope.AirInfo;
                    console.log(responce);
                });
            };

            // Get All Bike Points
            scope.GetAllBikePoints = function ($event) {
                // prevent double run
                if (scope.Busy) return;
                scope.Busy = true;

                //Get current button
                var button = $event.currentTarget;

                // Disable it
                angular.element(button).addClass("disabled");

                var promice = dataFactory.GetAllBikePoints();

                promice.then(function (responce) {
                    scope.Busy = false;

                    // remove Disabled
                    angular.element(button).removeClass("disabled");

                    scope.Data = responce.data;
                    console.log(responce);
                }, function (responce) {
                    alert('Error!');
                    scope.Data = false;

                    //remove Disabled
                    angular.element(button).removeClass("disabled");

                    delete scope.Data;
                    console.log(responce);
                });
            }


            // Get Bike Points by id
            scope.GetBikePointsById = function ($event, id) {
                // prevent double run
                if (scope.Busy) return;
                scope.Busy = true;
                scope.NotFound = false;

                //Get current button
                $$button = $event.currentTarget;

                // Disable it
                angular.element($$button).addClass("disabled");

                var promice = dataFactory.GetBikePointById(id);

                promice.then($$ResponceOk, $$ResponceError);
            }

            // get Bike points
            scope.GetBikePointsBySquare = function ($event, swLat, swLon, neLat, neLon) {
                // prevent double run
                if (scope.Busy) return;
                scope.Busy = true;
                scope.NotFound = false;

                //Get current button
                $$button = $event.currentTarget;

                // Disable it
                angular.element($$button).addClass("disabled");

                var promice = dataFactory.GetBikePointBySquare(swLat, swLon, neLat, neLon);

                promice.then($$ResponceOk, $$ResponceError);
            }

            // get Bike points
            scope.GetBikePointsByCircle = function ($event, lat, lon, radius) {
                // prevent double run
                if (scope.Busy) return;
                scope.Busy = true;
                scope.NotFound = false;

                //Get current button
                $$button = $event.currentTarget;

                // Disable it
                angular.element($$button).addClass("disabled");

                var promice = dataFactory.GetBikePointByCircle(lat, lon, radius);

                promice.then($$ResponceOk, $$ResponceError);
            }

            // get Bike points
            scope.GetBikePointsByName = function ($event, name) {
                // prevent double run
                if (scope.Busy) return;
                scope.Busy = true;
                scope.NotFound = false;

                //Get current button
                $$button = $event.currentTarget;

                // Disable it
                angular.element($$button).addClass("disabled");

                var promice = dataFactory.GetBikePointByName(name);

                promice.then($$ResponceOk, $$ResponceError);
            }

            // Responce for array BikePoints
            $$ResponceOk = function (responce) {
                scope.Busy = false;

                // remove Disabled
                angular.element($$button).removeClass("disabled");

                if (angular.isArray(responce.data)) {
                    if (responce.data.length === 0) {
                        scope.NotFound = true;
                        return;
                    }
                    scope.Data = responce.data;
                } else
                { scope.Data = [responce.data]; }
            };

            $$ResponceError = function (responce) {
                if (responce.status === 404) {
                    scope.NotFound = true;
                } else {
                    alert('Error!');
                }
                scope.Busy = false;
                scope.Data = false;

                //remove Disabled
                angular.element($$button).removeClass("disabled");

                delete scope.Data;
            };
        }
    }
}]);

app.directive('bikePoint',
    function () {
        return {
            templateUrl: 'Views/bikePoint.html',
            restrict: 'E',
            scope: {
                point: '='
            }
        }
    });

app.directive('forecast',['$sce',
    function($sce) {
        return {
            templateUrl: 'Views/forecast.html',
            restrict: 'E',
            scope: {
                dayforecast: '='
            },
            link: function(scope) {
                scope.forecastText = function () {
                    return $sce.trustAsHtml(angular.element('<textarea />').html(scope.dayforecast.forecastText).text());
                };
            }
        }
    }]);

// Data Service
app.factory("dataFactory", ['$http', function ($http) {
    return {
        GetAir: function () {
            return $http.get('api/AirQuality');
        },
        GetAllBikePoints: function () {
            return $http.get('api/BikePoint');
        },
        GetBikePointById: function (id) {
            return $http.get('api/BikePoint/ById',
            {
                params: {
                    'Id': id
                }
            });
        },
        GetBikePointBySquare: function (swLat, swLon, neLat, neLon) {
            return $http.get('api/BikePoint/ById',
            {
                params: {
                    'swLat': swLat,
                    'swLon': swLon,
                    'neLat': neLat,
                    'neLon': neLon
                }
            });
        },
        GetBikePointByCircle: function (lat, lon, radius) {
            return $http.get('api/BikePoint/ByCircle',
            {
                params: {
                    'lat': lat,
                    'lon': lon,
                    'radius': radius
                }
            });
        },
        GetBikePointByName: function (name) {
            return $http.get('api/BikePoint/ByName', {
                params: {
                    'name': name
                }
            });
        }

    }
}]);

