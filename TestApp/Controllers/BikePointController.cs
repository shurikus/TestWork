﻿using System;
using System.Threading.Tasks;
using DataSourceService.Abstract;
using DataSourceService.Interface;
using Microsoft.AspNetCore.Mvc;


namespace TestApp.Controllers
{
    [Route("api/[controller]")]
    public class BikePointController : Controller
    {
        private readonly ITransportForLondonUnifiedApi _transportApi;

        public BikePointController(ITransportForLondonUnifiedApi transportApi)
        {
            _transportApi = transportApi;
        }

        // GET api/BikePoint
        [HttpGet]
        public Task<string> Get() =>
            _transportApi.BikePoint();


        // GET api/BikePoint/id
        [HttpGet("{id}")]
        public Task<string> Get(string id) =>
             _transportApi.BikePoint(id);

        // Get api/BikePoint/ById
        [HttpGet, Route("ById")]
        public ActionResult GetById([FromQuery] string id)
        {
            string result;
            try
            {
                result = _transportApi.BikePoint(id).Result;
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException is NotFoundException)
                {
                    return NotFound();
                }
                throw ex.InnerException;
            }
            return Content(result);
        }

        // Get api/BikePoint/ById
        [HttpGet, Route("BySquare")]
        public ActionResult GetBySquare([FromQuery]string swLat, [FromQuery]string swLon, [FromQuery] string neLat, [FromQuery]string neLon)
        {
            string result;
            try
            {
                result = _transportApi.BikePoint(swLat, swLon, neLat, neLon).Result;
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException is NotFoundException)
                {
                    return NotFound();
                }
                return BadRequest(ex.Message);
            }
            return Content(result);
        }

        // Get api/BikePoint/ById
        [HttpGet, Route("ByCircle")]
        public ActionResult GetByCircle([FromQuery] string lat, [FromQuery] string lon, [FromQuery] string radius)
        {
            string result;
            try
            {
                result = _transportApi.BikePoint(lat, lon, radius).Result;
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException is NotFoundException)
                {
                    return NotFound();
                }
                return BadRequest(ex.Message);
            }
            return Content(result);
        }

        [HttpGet, Route("ByName")]
        public ActionResult GetByName([FromQuery] string name)
        {
            string result;
            try
            {
                result = _transportApi.BikePointByName(name).Result;
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException is NotFoundException)
                {
                    return NotFound();
                }
                 return BadRequest(ex.Message);
            }
            return Content(result);
        }

    }
}
