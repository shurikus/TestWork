﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataSourceService.Interface;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace TestApp.Controllers
{
    [Route("api/[controller]")]
    public class AirQualityController : Controller
    {
        private readonly ITransportForLondonUnifiedApi _transportApi;

        public AirQualityController(ITransportForLondonUnifiedApi api)
        {
            _transportApi = api;
        }

        // GET: api/AirQuality
        [HttpGet]
        public Task<string> Get() =>
            _transportApi.AirQuality();
    }
}
